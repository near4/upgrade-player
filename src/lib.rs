use near_sdk::borsh::{self, BorshDeserialize, BorshSerialize};
use near_sdk::{env, near_bindgen, BorshStorageKey, serde::{Deserialize, Serialize}};
use near_sdk::collections::{LookupMap, UnorderedSet};
use near_sdk::log;

near_sdk::setup_alloc!();

#[derive(BorshDeserialize, BorshSerialize, Deserialize, Serialize)]
#[serde(crate = "near_sdk::serde")]
pub enum HeroClass{
    SOL, 
    NEAR,
    BNB,
    ETH
}

#[derive(BorshDeserialize, BorshSerialize, Deserialize, Serialize)]
#[serde(crate = "near_sdk::serde")]
pub struct PlayerV1{
    name: String,
    heroClass: HeroClass,
    health: u8,
    level: u8
}

#[derive(BorshDeserialize, BorshSerialize)]
pub enum Player{
    CurrentVersion(PlayerV1)
}

#[derive(BorshDeserialize, BorshSerialize)]
pub struct Game{
    players: UnorderedSet<Player>,
    winner: Option<Player>
}


#[derive(BorshDeserialize, BorshSerialize)]
pub struct Contract{
    games: LookupMap<u32, Game>,
    games_active: bool
}

#[derive(BorshSerialize, BorshStorageKey)]
enum StorageKey{
    Game, 
    Player
}

impl Default for Contract{
    fn default() -> Self{
        Contract{
            games: LookupMap::new(StorageKey::Game),
            games_active: false
        }
    }
}


impl Contract{
    pub fn fight(){
        env::log(b"Fighting stub");
    }

    pub fn addPlayer( &mut self ,gameNum: u32, player: PlayerV1){
        let p = Player::CurrentVersion(player);
        let mut game = self.games.get(&gameNum).unwrap_or_else(||
        Game{
            players: UnorderedSet::new(StorageKey::Player) ,
            winner: None
        });
        game.players.insert(&p);
        self.games.insert(&gameNum, &game);
    }

    pub fn getGamePlayers(&self, gameNum: u32){
       let game = self.games.get(&gameNum).expect("Couldn't find game");
       game.players.to_vec();
    }

    pub fn startGame(&mut self ,gameNum: u32){
        log!("Starting game {} ", gameNum);
        self.games_active = true;
    }

    pub fn stopGame(&mut self ,gameNum: u32){
        log!("Starting game {} ", gameNum);
        self.games_active = false;
    }
}

